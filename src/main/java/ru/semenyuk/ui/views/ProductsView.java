package ru.semenyuk.ui.views;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.Route;
import ru.semenyuk.backend.entities.Product;
import ru.semenyuk.backend.services.ProductService;
import ru.semenyuk.ui.forms.ProductFormDialog;
import ru.semenyuk.ui.layouts.MenuBar;

@Route(value = "products", layout = MenuBar.class)
public class ProductsView extends AbstractGridView<Product> {

    public ProductsView() {
        super();
    }

    @Override
    public ProductService getService() {
        return ProductService.getInstance();
    }

    public void showCreateEditDialog(Product product) {
        if (product == null) {
            product = new Product();
        }
        ProductFormDialog dialog = new ProductFormDialog(product, dataProvider, getService());
        dialog.open();
    }

    void configureGrid() {
        grid = new Grid<>(Product.class);
        grid.getColumnByKey("name").setHeader("Наименование");
        grid.getColumnByKey("description").setHeader("Описание");
        grid.getColumnByKey("price").setHeader("Цена");
        grid.getColumnByKey("quantity").setHeader("Количество");
    }

}
