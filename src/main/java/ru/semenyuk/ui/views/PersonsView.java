package ru.semenyuk.ui.views;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.Route;
import ru.semenyuk.backend.entities.Person;
import ru.semenyuk.backend.services.PersonService;
import ru.semenyuk.ui.forms.PersonFormDialog;
import ru.semenyuk.ui.layouts.MenuBar;

@Route(value = "persons", layout = MenuBar.class)
public class PersonsView extends AbstractGridView<Person> {

    public PersonsView() {
        super();
    }

    public PersonService getService() {
        return PersonService.getInstance();
    }

    @Override
    void showCreateEditDialog(Person person) {
        if (person == null) {
            person = new Person();
        }
        PersonFormDialog dialog = new PersonFormDialog(person, dataProvider, getService());
        dialog.open();
    }

    @Override
    void configureGrid() {
        grid = new Grid<>(Person.class);
        grid.getColumnByKey("firstName").setHeader("Имя");
        grid.getColumnByKey("lastName").setHeader("Фамилия");
        grid.getColumnByKey("phone").setHeader("Телефон");
    }

}
