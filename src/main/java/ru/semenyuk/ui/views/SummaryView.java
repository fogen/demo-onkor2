package ru.semenyuk.ui.views;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.router.Route;
import ru.semenyuk.backend.entities.Delivery;
import ru.semenyuk.backend.services.DeliveryService;
import ru.semenyuk.ui.forms.DeliveryEditDialog;
import ru.semenyuk.ui.layouts.MenuBar;

@Route(value = "", layout = MenuBar.class)
public class SummaryView extends AbstractGridView<Delivery> {

    public SummaryView() {
    }

    @Override
    DeliveryService getService() {
        return DeliveryService.getInstance();
    }

    @Override
    void configureGrid() {
        grid = new Grid<>(Delivery.class);
        grid.removeColumnByKey("product");
        grid.removeColumnByKey("person");
        grid.getColumnByKey("date").setHeader("Дата");
        grid.getColumnByKey("address").setHeader("Адрес");
        grid.addColumn(TemplateRenderer.<Delivery>of("[[item.person.lastName]] [[item.person.firstName]]")
                .withProperty("person", Delivery::getPerson))
                .setHeader("Покупатель");
        grid.addColumn(TemplateRenderer.<Delivery>of("[[item.product.name]]")
                .withProperty("product", Delivery::getProduct))
                .setHeader("Продукт");
    }

    @Override
    void showCreateEditDialog(Delivery item) {
        if (item == null) {
            item = new Delivery();
        }

        DeliveryEditDialog editDialog = new DeliveryEditDialog(item, dataProvider, getService());
        editDialog.open();
    }
}
