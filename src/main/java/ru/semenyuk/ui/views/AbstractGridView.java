package ru.semenyuk.ui.views;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import ru.semenyuk.backend.entities.AbstractEntity;
import ru.semenyuk.backend.services.AbstractEntityService;

import java.util.List;

public abstract class AbstractGridView<T extends AbstractEntity> extends VerticalLayout {

    private List<T> items;

    ListDataProvider<T> dataProvider;
    Grid<T> grid;

    public AbstractGridView() {
        HorizontalLayout buttonsPanel = new HorizontalLayout();
        Button create = new Button("Добавить", e -> showCreateEditDialog(null));
        buttonsPanel.add(create);
        add(buttonsPanel);
        items = getService().list();
        dataProvider = DataProvider.ofCollection(items);
        dataProvider.addDataProviderListener(e -> {
            dataProvider.refreshAll();
        });
        configureGrid();
        grid.setDataProvider(dataProvider);
        grid.setRowsDraggable(false);
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);
        grid.addItemDoubleClickListener(event -> showCreateEditDialog(grid.asSingleSelect().getValue()));
        grid.removeColumnByKey("id");
        add(grid);
    }

    abstract AbstractEntityService<T> getService();

    abstract void configureGrid();

    abstract void showCreateEditDialog(T item);

}
