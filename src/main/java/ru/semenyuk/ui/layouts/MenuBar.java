package ru.semenyuk.ui.layouts;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.TabVariant;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.router.*;
import ru.semenyuk.ui.views.SummaryView;
import ru.semenyuk.ui.views.PersonsView;
import ru.semenyuk.ui.views.ProductsView;

import java.util.Arrays;
import java.util.Optional;

@ParentLayout(MainLayout.class)
public class MenuBar extends Div implements RouterLayout, AfterNavigationObserver {

    private final Tabs menu;

    public MenuBar() {
        menu = createMenuTabs();
        setSizeFull();
        add(menu);
    }

    private static Tabs createMenuTabs() {
        final Tabs tabs = new Tabs();
        tabs.getStyle().set("max-width", "100%");
        tabs.add(getAvailableTabs());
        return tabs;
    }

    private static Tab[] getAvailableTabs() {
        RouterLink[] links = new RouterLink[]{
                new RouterLink("Главная", SummaryView.class),
                new RouterLink("Товары", ProductsView.class),
                new RouterLink("Покупатели", PersonsView.class)
        };
        return Arrays.stream(links).map(MenuBar::createTab).toArray(Tab[]::new);
    }

    private static Tab createTab(Component content) {
        final Tab tab = new Tab();
        tab.addThemeVariants(TabVariant.LUMO_ICON_ON_TOP);
        tab.add(content);
        return tab;
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        String target = event.getLocation().getFirstSegment();
        Optional<Component> tabToSelect = menu.getChildren().filter(tab -> {
            Component child = tab.getChildren().findFirst().get();
            return child instanceof RouterLink && ((RouterLink) child).getHref().equals(target);
        }).findFirst();
        tabToSelect.ifPresent(tab -> menu.setSelectedTab((Tab) tab));
    }
}
