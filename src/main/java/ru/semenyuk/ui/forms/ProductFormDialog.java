package ru.semenyuk.ui.forms;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToBigDecimalConverter;
import com.vaadin.flow.data.converter.StringToIntegerConverter;
import com.vaadin.flow.data.provider.ListDataProvider;
import ru.semenyuk.backend.entities.Product;
import ru.semenyuk.backend.services.ProductService;

import java.math.BigDecimal;

public class ProductFormDialog extends AbstractEditDialog<Product> {

    private TextField nameField = new TextField("Наименование");
    private TextArea descriptionField = new TextArea("Описание");
    private TextField priceField = new TextField("Цена");
    private TextField quantityField = new TextField("Количество");
    private ProductService productService;

    public ProductFormDialog(Product product, ListDataProvider<Product> dataProvider, ProductService productService) {
        super(dataProvider, product);
        this.productService = productService;
    }

    @Override
    protected Component[] getFields() {
        return new Component[]{nameField, descriptionField, priceField, quantityField};
    }

    void configureBinder(Product product) {
        nameField = new TextField("Наименование");
        descriptionField = new TextArea("Описание");
        priceField = new TextField("Цена");
        quantityField = new TextField("Количество");

        binder = new Binder<>(Product.class);
        binder.setBean(product);
        binder.forField(priceField)
                .withConverter(new StringToBigDecimalConverter("Необходимо ввести число"))
                .withValidator(price -> price.compareTo(BigDecimal.ZERO) > 0, "Цена должна быть больше нуля")
                .bind(Product::getPrice, Product::setPrice);
        binder.forField(quantityField)
                .withConverter(new StringToIntegerConverter("Необходимо ввести число"))
                .withValidator(quantity -> quantity >= 0, "Количество не может быть отрицательным")
                .bind(Product::getQuantity, Product::setQuantity);
        binder.forField(nameField)
                .withValidator(name -> name.length() >= 3, "Наименование не может быть менее 3 символов")
                .bind(Product::getName, Product::setName);
        binder.forField(descriptionField)
                .withValidator(description -> description.length() >= 3, "Описание не может быть менее 3 символов")
                .bind(Product::getDescription, Product::setDescription);
        binder.addValueChangeListener(e -> saveButton.setEnabled(binder.isValid()));
    }

    public ProductService getItemService() {
        return productService;
    }

}
