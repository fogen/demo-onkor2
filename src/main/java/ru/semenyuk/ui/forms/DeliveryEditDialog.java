package ru.semenyuk.ui.forms;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import ru.semenyuk.backend.entities.Delivery;
import ru.semenyuk.backend.entities.Person;
import ru.semenyuk.backend.entities.Product;
import ru.semenyuk.backend.services.DeliveryService;
import ru.semenyuk.backend.services.PersonService;
import ru.semenyuk.backend.services.ProductService;

public class DeliveryEditDialog extends AbstractEditDialog<Delivery> {

    private DeliveryService deliveryService;

    private DatePicker date;
    private ComboBox<Product> products;
    private ComboBox<Person> persons;
    private TextField address;

    public DeliveryEditDialog(Delivery delivery, ListDataProvider<Delivery> dataProvider, DeliveryService deliveryService) {
        super(dataProvider, delivery);
        this.deliveryService = deliveryService;
    }

    @Override
    protected Component[] getFields() {
        return new Component[]{date, products, persons, address};
    }

    @Override
    void configureBinder(Delivery item) {
        date = new DatePicker("Дата");
        products = new ComboBox<>("Продукт");
        persons = new ComboBox<>("Покупатель");
        address = new TextField("Алрес");

        products.setItems(ProductService.getInstance().list());
        products.setItemLabelGenerator(Product::getName);
        persons.setItems(PersonService.getInstance().list());
        persons.setItemLabelGenerator(Person::toString);

        binder = new Binder<>(Delivery.class);
        binder.setBean(item);
        binder.forField(date)
                .bind(Delivery::getDate, Delivery::setDate);
        binder.forField(products)
                .bind(Delivery::getProduct, Delivery::setProduct);
        binder.forField(persons)
                .bind(Delivery::getPerson, Delivery::setPerson);
        binder.forField(address)
                .withValidator(a -> a.length() >= 10, "Адрес не может быть таким коротким")
                .bind(Delivery::getAddress, Delivery::setAddress);
        binder.addValueChangeListener(e -> saveButton.setEnabled(binder.isValid()));
    }

    @Override
    protected DeliveryService getItemService() {
        return deliveryService;
    }
}
