package ru.semenyuk.ui.forms;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import ru.semenyuk.backend.entities.AbstractEntity;
import ru.semenyuk.backend.services.AbstractEntityService;

public abstract class AbstractEditDialog<T extends AbstractEntity> extends Dialog {

    Button saveButton = new Button("Сохранить");
    Button cancel = new Button("Отмена");

    Binder<T> binder;
    ListDataProvider<T> dataProvider;

    protected AbstractEditDialog(ListDataProvider<T> dataProvider, T item) {
        this.dataProvider = dataProvider;
        saveButton.setEnabled(false);
        saveButton.addClickListener(e -> {
            if (binder.validate().isOk()) {
                boolean isNew = binder.getBean().getId() == null;
                getItemService().save(binder.getBean());
                close();
            }
        });
        cancel.addClickShortcut(Key.ESCAPE);
        cancel.addClickListener(e -> close());

        configureBinder(item);

        VerticalLayout layout = new VerticalLayout(getFields());
        layout.add(new HorizontalLayout(saveButton, cancel));

        add(layout);
    }

    protected abstract Component[] getFields();

    abstract void configureBinder(T item);

    protected abstract AbstractEntityService<T> getItemService();

}
