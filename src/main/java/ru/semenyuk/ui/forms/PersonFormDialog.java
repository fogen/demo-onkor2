package ru.semenyuk.ui.forms;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.validator.EmailValidator;
import ru.semenyuk.backend.entities.Person;
import ru.semenyuk.backend.services.PersonService;

public class PersonFormDialog extends AbstractEditDialog<Person> {

    private TextField firstName;
    private TextField lastName;
    private EmailField email;
    private TextField phone;
    private PersonService personService;

    public PersonFormDialog(Person person, ListDataProvider<Person> dataProvider, PersonService personService) {
        super(dataProvider, person);
        this.personService = personService;
    }

    @Override
    protected Component[] getFields() {
        return new Component[]{firstName, lastName, email, phone};
    }

    void configureBinder(Person person) {
        firstName = new TextField("Имя");
        lastName = new TextField("Фамилия");
        email = new EmailField("email");
        phone = new TextField("Телефон");

        binder = new Binder<>(Person.class);
        binder.setBean(person);
        binder.forField(email)
                .withValidator(new EmailValidator("Неверный формат email"))
                .bind(Person::getEmail, Person::setEmail);
        binder.forField(firstName)
                .withValidator(name -> name.length() >= 3, "Имя не может быть менее 3 символов")
                .bind(Person::getFirstName, Person::setFirstName);
        binder.forField(lastName)
                .withValidator(name -> name.length() >= 3, "Фамилия не может быть менее 3 символов")
                .bind(Person::getLastName, Person::setLastName);
        binder.forField(phone)
                .withValidator(p -> p.matches("^((\\+7|7|8)+([0-9]){10})$"), "Неверный фоомат телефона")
                .bind(Person::getPhone, Person::setPhone);
        binder.addValueChangeListener(e -> saveButton.setEnabled(binder.isValid()));
    }

    @Override
    public PersonService getItemService() {
        return personService;
    }

}
