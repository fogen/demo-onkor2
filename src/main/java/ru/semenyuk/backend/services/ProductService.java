package ru.semenyuk.backend.services;

import ru.semenyuk.backend.entities.Product;

import java.math.BigDecimal;
import java.util.List;

public class ProductService extends AbstractEntityService<Product> {

    private static ProductService instance;

    public static ProductService getInstance() {
        if (instance == null) {
            instance = new ProductService();
        }

        return instance;
    }

    @Override
    public List<Product> list() {
        if (super.list().isEmpty()) {
            for (int i = 1; i < 5; i++) {
                Product product = new Product();
                product.setName("Тестовый товар" + i);
                product.setDescription("Описание товара для теста " + i);
                product.setPrice(BigDecimal.valueOf(11.22 * i));
                product.setQuantity(i + 2);
                save(product);
            }
        }
        return super.list();
    }
}
