package ru.semenyuk.backend.services;

import ru.semenyuk.backend.entities.Delivery;

public class DeliveryService extends AbstractEntityService<Delivery> {

    private static DeliveryService instance;

    public static DeliveryService getInstance() {
        if (instance == null) {
            instance = new DeliveryService();
        }

        return instance;
    }
}
