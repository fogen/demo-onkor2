package ru.semenyuk.backend.services;

import ru.semenyuk.backend.entities.AbstractEntity;

import java.util.*;

public abstract class AbstractEntityService<T extends AbstractEntity> {

    static AbstractEntityService instance;
    private Map<String, T> items;

    protected AbstractEntityService() {
        this.items = new HashMap<>();
    }

    protected static AbstractEntityService getInstance() {
        return instance;
    }

    public List<T> list() {
        return items.isEmpty() ? Collections.emptyList() : new ArrayList<>(items.values());
    }

    public void save(T item) {

        if (item.getId() != null) {
            items.replace(item.getId(), item);
        } else {
            item.setId(UUID.randomUUID().toString());
            items.put(item.getId(), item);
        }
    }

    public void delete(T item) {
        items.remove(item.getId());
    }
}
