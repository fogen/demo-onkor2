package ru.semenyuk.backend.services;

import ru.semenyuk.backend.entities.Person;

import java.util.List;

public class PersonService extends AbstractEntityService<Person> {

    private static PersonService instance;

    public static PersonService getInstance() {
        if (instance == null) {
            instance = new PersonService();
        }

        return instance;
    }

    @Override
    public List<Person> list() {
        if (super.list().isEmpty()) {
            for (int i = 1; i < 5; i++) {
                Person person = new Person();
                person.setFirstName("Иван");
                person.setLastName("Иванов");
                person.setEmail("ivan@mail.ru");
                person.setPhone("89021231212");
                save(person);
            }
        }
        return super.list();
    }
}
