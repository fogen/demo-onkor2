package ru.semenyuk.backend.entities;

import java.time.LocalDate;

public class Delivery extends AbstractEntity {

    private LocalDate date;
    private Product product;
    private Person person;
    private String address;

    public Delivery() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
