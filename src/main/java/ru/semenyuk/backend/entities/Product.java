package ru.semenyuk.backend.entities;

import java.math.BigDecimal;

public class Product extends AbstractEntity implements Cloneable {

    private String name;
    private String description;
    private BigDecimal price = BigDecimal.ZERO;
    private Integer quantity = 0;

    public Product() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
