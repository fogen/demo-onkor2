package ru.semenyuk.backend.entities;

public abstract class AbstractEntity {

    private String id;

    public AbstractEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
